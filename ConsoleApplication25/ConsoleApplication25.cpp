﻿#include <iostream>
#include <string>
#include <fstream>
#include "Header.h"
#include "Header2.h"

using namespace std;

int main()
{
    setlocale(LC_ALL, "RU");
    Data data[6];
    for (int i = 0; i < 6; i++)
        data[i] = insertData();
    int min;
    int n = 6;
    cout << "0-все разговоры на мобильном 1 - все звонки в ноябре\n";
    cin >> min;
    if (min == 0)
    {
        for (int i = 0; i < n; i++)
        {
            if (data[i].cost == "0.15")
            {
                outData(data[i]);
                cout << endl;
            }
        }
    }
    if (min == 1)
    {
        for (int i = 0; i < n; i++)
        {
            if (data[i].data1 <= "01.11.2021")
            {
                outData(data[i]);
                cout << endl;
            }
        }
    }
    cout << endl;
    string m;
    cout << "Введите сортировку" << endl;
    cout << "1.Пирамидальная сортировка" << endl;
    cout << "2.Быстрая сортировка" << endl;
    int f;
    cin >> f;
    cout << "Введите категорию" << endl;
    cout << "1.По убыванию продолжительности разговора" << endl;
    cout << "2.По возрастанию номера телефона, а в рамках одного номера по убыванию стоимости разговора" << endl;
    int g;
    cin >> g;
    if (f == 1 && g == 1)
    {
        for (int i = 0; i < n - 1; i++)
        {
            for (int i = 0; i < n - 1; i++)
            {
                if (data[i].duration < data[i + 1].duration)
                {
                    m = data[i].duration;
                    data[i].duration = data[i + 1].duration;
                    data[i + 1].duration = m;
                    m = data[i].time;
                    data[i].time = data[i + 1].time;
                    data[i + 1].time = m;
                    m = data[i].Number;
                    data[i].Number = data[i + 1].Number;
                    data[i + 1].Number = m;
                    m = data[i].city;
                    data[i].city = data[i + 1].city;
                    data[i + 1].city = m;
                    m = data[i].data1;
                    data[i].data1 = data[i + 1].data1;
                    data[i + 1].data1 = m;
                    m = data[i].cost;
                    data[i].cost = data[i + 1].cost;
                    data[i + 1].cost = m;
                }
            }
        }
        for (int i = 0; i < n; i++)
        {
            outData(data[i]);
            cout << endl;
        }
    }
    if (f == 1 && g == 2)
    {
        for (int i = 0; i < n - 1; i++)
        {
            for (int i = 0; i < n - 1; i++)
            {
                if (data[i].Number > data[i + 1].Number)
                {
                    m = data[i].duration;
                    data[i].duration = data[i + 1].duration;
                    data[i + 1].duration = m;
                    m = data[i].time;
                    data[i].time = data[i + 1].time;
                    data[i + 1].time = m;
                    m = data[i].Number;
                    data[i].Number = data[i + 1].Number;
                    data[i + 1].Number = m;
                    m = data[i].city;
                    data[i].city = data[i + 1].city;
                    data[i + 1].city = m;
                    m = data[i].data1;
                    data[i].data1 = data[i + 1].data1;
                    data[i + 1].data1 = m;
                    m = data[i].cost;
                    data[i].cost = data[i + 1].cost;
                    data[i + 1].cost = m;
                }
            }
        }
        for (int i = 0; i < n; i++)
        {
            outData(data[i]);
            cout << endl;
        }
    }
    if (f == 2 && g == 1)
    {
        for (int i = 0; i < n; i++)
        {

            int j = i;
            while (j > 0 && data[j-1].duration < data[j].duration)
            {
                string tmp = data[j].duration;
                data[j].duration = data[j-1].duration;
                data[j - 1].duration = tmp;
                string tmpp = data[j].time;
                data[j].time = data[j - 1].time;
                data[j - 1].time = tmpp;
                string tmppp = data[j].Number;
                data[j].Number = data[j - 1].Number;
                data[j - 1].Number = tmppp;
                string tmpppp = data[j].city;
                data[j].city = data[j - 1].city;
                data[j - 1].city = tmpppp;
                string tmppppp = data[j].data1;
                data[j].data1 = data[j - 1].data1;
                data[j - 1].data1 = tmppppp;
                string tmpppppp = data[j].cost;
                data[j].cost = data[j - 1].cost;
                data[j - 1].cost = tmpppppp;
                j--;
            }
        }
        for (int i = 0; i < n; i++)
        {
            outData(data[i]);
            cout << endl;
        }
    }
    if (f == 2 && g == 2)
    {
        for (int i = 0; i < n; i++)
        {

            int j = i;
            while (j > 0 && data[j - 1].Number > data[j].Number)
            {
                string tmp = data[j].duration;
                data[j].duration = data[j - 1].duration;
                data[j - 1].duration = tmp;
                string tmpp = data[j].time;
                data[j].time = data[j - 1].time;
                data[j - 1].time = tmpp;
                string tmppp = data[j].Number;
                data[j].Number = data[j - 1].Number;
                data[j - 1].Number = tmppp;
                string tmpppp = data[j].city;
                data[j].city = data[j - 1].city;
                data[j - 1].city = tmpppp;
                string tmppppp = data[j].data1;
                data[j].data1 = data[j - 1].data1;
                data[j - 1].data1 = tmppppp;
                string tmpppppp = data[j].cost;
                data[j].cost = data[j - 1].cost;
                data[j - 1].cost = tmpppppp;
                j--;
            }
        }
        for (int i = 0; i < n; i++)
        {
            outData(data[i]);
            cout << endl;
        }
    }
}